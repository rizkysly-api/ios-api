//
//  RizkySlyAPI.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 21/10/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import UIKit

@objc public final class RizkySlyAPI: NSObject {
    @objc public static let shared = RizkySlyAPI()

    private var notifications : Notifications?
    
    // Fresh app start
    @objc public func start() {
        if notifications == nil {
            notifications = Notifications()
        }
        Statistics.shared.send()
        RequestQueue.shared.sendQueue()
        
        if !RequestQueue.shared.inqueue(conf: Conf().storage) {
            StorageFiles.shared.syncFiles()
        }
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name("CloudStorageContainerChanged"), object: self, userInfo: nil)
        }
    }

    // Just before stopping the app
    @objc public func applicationWillResignActive() {
        Statistics.shared.send()
        RequestQueue.shared.sendQueue()
    }
    
    // Called when returning from background
    @objc public func applicationWillEnterForeground() {
        start()
        Statistics.shared.resume()
    }
}

