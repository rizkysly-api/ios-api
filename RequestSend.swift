//
//  RequestSend.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 27/10/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import Foundation
import CommonCrypto

class RequestSend {

    static func send(conf: ConfigurationItem, path: String, array: [String: String]) {
        send(conf: conf, path: path, array: array){ (data, response) -> () in
            // Nothing here, this function is for the occation we don't care about the completion handler
        }
    }
    
    static func send(conf: ConfigurationItem, path: String, array: [String: String], completion: @escaping ( _ data: Data?, _ response: HTTPURLResponse?)->()){

        // If we have no key, there is no point of sending anything
        if conf.key.count == 0 {
            return
        }
        
        // Sign the data
        var arrayData = array
        arrayData["device"] = device()
        arrayData["timestamp"] = "\(UInt64(NSDate().timeIntervalSince1970 * 1000.0))"
        
        // Convert to querystring
        var querystring = httpBuildQuery(data: arrayData)
        querystring += "&signature=" + generateHMAC(conf: conf, string: querystring)
        
        // Create the request
        let url = URL(string: conf.url + path)
        if let url = url {
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue(conf.key, forHTTPHeaderField: "X-APPLICATION-KEY")
            request.httpMethod = "POST"
            request.httpBody = querystring.data(using: .utf8)
            
            // Send it on
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                
                // Check for fundamental networking error
                guard let data = data, error == nil else {
                    print("😡 \(Date()) - Error sending data ", error as Any)
                    completion(nil, nil)
                    return
                }
                
                // Check for http errors
                if let response = response as? HTTPURLResponse {
                    
                    if response.statusCode != 200 {
                        print("😡 \(Date()) - Error sending data with response ", response)
                    }
                    
                    // 409 codes are special, the container uuid already exist and should be changed
                    if response.statusCode == 409,
                        let container = array["container"] {
                        StorageContainers.shared.resetContainerUuid(uuid: container)
                    }
                    
                    // 204 codes are special too, the container has been deleted by a device
                    if response.statusCode == 204,
                        let container = array["container"] {
                        StorageContainers.shared.deleteContainer(uuid: container)
                    }
                    
                    // 210 the container was deleted intentionally, perhaps because of an inactivity clean-up
                    if response.statusCode == 410,
                        let container = array["container"] {
                        _ = StorageContainers.shared.resetContainer(uuid: container, deleteonline: false)
                    }

                    completion(data, response)
                    return
                }
                
                completion(data, nil)
            }
            task.resume()
        }
    }
    
    // What will identify this device?
    private static func device() -> String {
        if let device = UserDefaults.standard.string(forKey: "device") {
            return device
        }
        
        let newDevice = NSUUID().uuidString.lowercased()
        UserDefaults.standard.set(newDevice, forKey: "device")
        return newDevice
    }
    
    // With the help of https://stackoverflow.com/a/29799802
    private static func generateHMAC(conf: ConfigurationItem, string: String) -> String {
        
        var result: [CUnsignedChar] = []
        if let cKey = conf.secret.cString(using: .utf8),
            let cData = string.cString(using: .utf8)
        {
            let algo  = CCHmacAlgorithm(kCCHmacAlgSHA512)
            result = Array(repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
            
            CCHmac(algo, cKey, cKey.count-1, cData, cData.count-1, &result)
        }
        
        let hash = NSMutableString()
        for val in result {
            hash.appendFormat("%02hhx", val)
        }
        
        return hash as String
    }
    
    private static func httpBuildQuery(data: [String:String]) -> String {
        var components = URLComponents()
        components.queryItems = data.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        
        if let query = components.query {
            return query
        }
        
        return ""
    }
    
}

