//
//  StorageContainers.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 08/10/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import UIKit
import CryptoKit

final class StorageContainers {
    static let shared = StorageContainers()
    
    struct Container: Codable {
        var uuid : String = NSUUID().uuidString.lowercased()
        var key : String = SymmetricKey(size: .bits256).withUnsafeBytes {Data(Array($0)).base64EncodedString()}
        var time : Date = Date()
        var name : String = ""
        var visible : Bool? = true
        var local : Bool = true
    }
    
    let file = "containers.json"
    var list : [Container] = []
    var dir = Conf().dir
    
    private init() {
        readFile()
    }
    
    // MARK: Containers.json manipulation
    private func readFile(){
        let fm = FileManager.default
        if let dir = dir,
            fm.fileExists(atPath: dir.appendingPathComponent(file).path) == false {
            createFile()
        }
        
        let jsonDecoder = JSONDecoder()
        do {
            if let dir = dir {
                let jsonData = try Foundation.Data(contentsOf: dir.appendingPathComponent(file), options: .mappedIfSafe)
                self.list = try jsonDecoder.decode([Container].self, from: jsonData)
            }
        }
        catch {
            createFile()
        }
        
        if self.list.count == 0 {
            createFile()
        }
    }
    
    private func writeFile(refresh: Bool = true){
        let jsonEncoder = JSONEncoder()
        if let jsonData = try? jsonEncoder.encode(list) {
            if let dir = dir {
                try? jsonData.write(to: dir.appendingPathComponent(file), options: .completeFileProtection)
            }
        }
        
        if refresh{
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: Notification.Name("CloudStorageContainerChanged"), object: self, userInfo: nil)
            }
        }
    }
    
    private func createFile(){
        addContainer(name: NSLocalizedString(Conf().defaultContainerName, comment: ""))
        writeFile()
    }
    
    // MARK: Container manipulation
    public func addContainer(name: String) {
        var container = Container()
        container.name = name
        
        list.append(container)
        
        writeFile()
        
        createFolder(uuid: container.uuid)
    }
    
    public func addContainer(json: String, name: String) -> Bool {
        let jsonDecoder = JSONDecoder()
        
        if let jsonData = json.data(using: .utf8),
            var container = try? jsonDecoder.decode(Container.self, from: jsonData){
            
            if let index = list.firstIndex(where: { $0.uuid == container.uuid }) {
                list[index].name = name.count > 0 ? name:container.name
            }
            else{
                if name.count > 0 {
                    container.name = name
                }
                
                list.append(container)
                
                createFolder(uuid: container.uuid)
            }
            
            writeFile()
            
            StorageFiles.shared.syncFiles()

            return true
        }
        return false
    }
    
    public func copyContainer(uuid: String) {
        if let index = list.firstIndex(where: { $0.uuid == uuid }) {
            
            var container = Container()
            container.name = list[index].name
            
            list.append(container)
            
            writeFile()
            
            copyFolder(uuid: list[index].uuid, newUuid: container.uuid)
        }
    }
    
    public func renameContainer(uuid: String, name: String) {
        if let index = list.firstIndex(where: { $0.uuid == uuid }) {
            list[index].name = name
            writeFile()
            qrReset(uuid: uuid)
        }
    }
    
    public func showContainer(uuid: String, visible: Bool) {
        if let index = list.firstIndex(where: { $0.uuid == uuid }) {
            list[index].visible = visible
            writeFile(refresh: false)
        }
    }
    
    public func shareContainer(uuid: String) {
        if let index = list.firstIndex(where: { $0.uuid == uuid }) {
            list[index].local = false
            writeFile(refresh: false)
            
            RequestQueue.shared.addToQueue(conf: Conf().storage, path: "/addcontainer", array: ["container" : uuid])
            StorageFiles.shared.addContainer(container: uuid)
        }
    }
    
    public func resetContainer(uuid: String, deleteonline: Bool) -> String {
        if let index = list.firstIndex(where: { $0.uuid == uuid }) {
            let newUuid = NSUUID().uuidString.lowercased()
            
            qrReset(uuid: uuid)
            renameFolder(uuid: list[index].uuid, newUuid: newUuid)
            
            list[index].uuid = newUuid
            list[index].local = true
            
            writeFile(refresh: false)

            if deleteonline{
                Notifications.planNotifications(local: false, container: uuid, list: [])
                RequestQueue.shared.addToQueue(conf: Conf().storage, path: "/deletecontainer", array: ["container" : uuid])
            }
            
            return newUuid
        }
        return uuid
    }
    
    public func resetContainerUuid(uuid: String) {
        if let index = list.firstIndex(where: { $0.uuid == uuid }) {
            let newUuid = NSUUID().uuidString.lowercased()
            
            qrReset(uuid: uuid)
            renameFolder(uuid: list[index].uuid, newUuid: newUuid)
            
            list[index].uuid = newUuid
            writeFile()

            shareContainer(uuid: newUuid)
            
            // Changing the uuid of a container can be important information for other parts of the app
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: Notification.Name("CloudStorageContainerChangedUuid"), object: self, userInfo: ["uuid": uuid, "newUuid": newUuid])
            }
        }
    }
    
    public func deleteContainer(uuid: String){
        if let index = list.firstIndex(where: { $0.uuid == uuid }) {
            deleteContainerFolder(uuid: list[index].uuid)
            
            let isLocal = list[index].local
            
            list.remove(at: index)
            
            writeFile()
            
            if !isLocal {
                // Deletion of a container could be initiated from the server!
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Notification.Name("CloudStorageContainerDeleted"), object: self, userInfo: ["uuid": uuid])
                }
            }
        }
    }
    
    private func jsonContainer(uuid: String) -> String {
        return jsonContainer(uuid: uuid, alternatename: "")
    }
    
    private func jsonContainer(uuid: String, alternatename: String) -> String {
        let jsonEncoder = JSONEncoder()
        if let index = list.firstIndex(where: { $0.uuid == uuid }){

            var item = list[index]
            if alternatename.count > 0 {
                item.name = alternatename
                item.local = false
            }
            
            if let jsonData = try? jsonEncoder.encode(item),
                let jsonString = String(data: jsonData, encoding: .utf8) {
                return jsonString
            }
        }
        return "{}"
    }

    private func qrReset(uuid: String) {
        if let dir = dir {
            let filename = dir.appendingPathComponent(uuid).appendingPathComponent(".qr.png")
            let fm = FileManager.default
            if fm.fileExists(atPath: filename.path) {
                try? fm.removeItem(atPath: filename.path)
            }
        }
    }

    public func qrContainer(uuid: String, width: CGFloat) -> UIImage? {
        return qrContainer(uuid: uuid, width: width, alternatename: "")
    }

    public func qrContainer(uuid: String, width: CGFloat, alternatename: String) -> UIImage? {

        guard let dir = dir else {
            return UIImage()
        }
        let filename = dir.appendingPathComponent(uuid).appendingPathComponent(".qr.png")
        let fm = FileManager.default
        if fm.fileExists(atPath: filename.path) {
            return UIImage(contentsOfFile: filename.path)
        }
        
        let scale = width / 43.0
        let string = jsonContainer(uuid: uuid, alternatename: alternatename)
        let data = string.data(using: .utf8)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            
            let transform = CGAffineTransform(scaleX: scale, y: scale)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                let context = CIContext()
                let cgImage = context.createCGImage(output, from: output.extent)

                let image = UIImage(cgImage: cgImage!)
                
                if let data = image.pngData() {
                    try? data.write(to: filename)
                }
                
                return image
            }
        }
        
        return UIImage()
    }

    // MARK: Each container should have it's own folder
    public func createFolder(uuid: String){
        let fm = FileManager.default
        if let dir = Conf().dir,
            !fm.fileExists(atPath: dir.appendingPathComponent(uuid).path) {
            try? fm.createDirectory(at: dir.appendingPathComponent(uuid), withIntermediateDirectories: false, attributes: [:])
        }
    }
    
    private func renameFolder(uuid: String, newUuid: String){
        let fm = FileManager.default
        if let dir = Conf().dir {
            if fm.fileExists(atPath: dir.appendingPathComponent(uuid).path) {
                try? fm.moveItem(at: dir.appendingPathComponent(uuid), to: dir.appendingPathComponent(newUuid))
            }
            else{
                createFolder(uuid: newUuid)
            }
        }
    }
    
    private func copyFolder(uuid: String, newUuid: String){
        let fm = FileManager.default
        if let dir = Conf().dir {
            if fm.fileExists(atPath: dir.appendingPathComponent(uuid).path) {
                try? fm.copyItem(at: dir.appendingPathComponent(uuid), to: dir.appendingPathComponent(newUuid))
            }
            else{
                createFolder(uuid: newUuid)
            }
        }
    }
    
    private func deleteContainerFolder(uuid: String){
        let fm = FileManager.default
        if let dir = Conf().dir {
            try? fm.removeItem(atPath: dir.appendingPathComponent(uuid).path)
        }
    }
}


