//
//  Notifications.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 15/11/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import UIKit
import UserNotifications

public struct NotificationItem: Codable {
    var title : String = ""
    var body : String = ""
    var badge : Int = 1
    var time : TimeInterval = Date().timeIntervalSince1970
    var sound : String = "default"
}

public class Notifications: NSObject, UNUserNotificationCenterDelegate {
    
    override init(){
        super.init()
        
        let center = UNUserNotificationCenter.current()
        
        center.delegate = self
        center.requestAuthorization(options:[.badge, .alert, .sound, .provisional]) { (success, error) in
            if error == nil && success == true {
                #if !EXTENTION
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                #endif
            }
        }
    }
    
    //MARK: Token
    
    // Store the token on the notification server
    @objc public static func store(tokenData: Data){
        let token = tokenData.reduce("", {$0 + String(format: "%02X", $1)})
        
        let status = notificationStatus()
        if status.count > 0 {
            RequestQueue.shared.addToQueue(conf: Conf().notifications, path: "/token", array: ["service": Conf().notificationsEnvironment, "token": token, "status": status])
        }
    }
    
    //MARK: Set notifications
    
    // Apparently you can't use Swift structs in Objective-C, So this is a little translation function
    @objc public static func planNotifications(local: Bool, container: String, array: NSArray){
        guard let arr = array as? [[String: Any]] else {
            return
        }
        
        var list : [NotificationItem] = []
        
        for dict in arr {
            if let title = dict["title"] as? String,
                let body = dict["body"] as? String,
                let badge = dict["badge"] as? Int,
                let time = dict["time"] as? Date {
                list.append(NotificationItem(title: title, body: body, badge: badge, time: time.timeIntervalSince1970))
            }
        }
        
        self.planNotifications(local: true, container: container, list: list)
    }
    
    private static func notificationStatus() -> String{
        var status = ""
        let semasphore = DispatchSemaphore(value: 0)
        
        DispatchQueue.global().async {
            let center = UNUserNotificationCenter.current()
            center.getNotificationSettings { (settings) in
                switch settings.authorizationStatus {
                    case .notDetermined:
                        status = "notDetermined"
                    case .denied:
                        status = "denied"
                    case .authorized:
                        status = "authorized"
                    case .provisional:
                        status = "provisional"
                    case .ephemeral:
                        status = "ephemeral"
                    @unknown default:
                        status = "authorized"
                }
                semasphore.signal()
            }
        }
        semasphore.wait()
        
        return status
    }
    
    private static func wantsnotification() -> Bool {
        let status = notificationStatus()
        if status == "authorized" || status == "provisional" {
            return true
        }
        return false
    }
    
    public static func badge(badge: Int){
        #if !EXTENTION
        UIApplication.shared.applicationIconBadgeNumber = badge
        #endif
        
        RequestQueue.shared.addToQueue(conf: Conf().notifications, path: "/badge", array: ["service": Conf().notificationsEnvironment, "badge": String(badge)])
    }
    
    // If using multiple devices, local notifications won't work. We'll use remote notifications in stead
    public static func planNotifications(local: Bool, container: String, list: [NotificationItem]){
        // To get the users attention, we need the correct authorisation
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound, .provisional]) { (success, error) in
            if error == nil && success == true {
                #if !EXTENTION
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                #endif
            }
        }
        
        if local {
            self.removeLocalNotifications(container: container)
        }
        else{
            self.removeRemoteNotifications(container: container)
        }
        
        if !wantsnotification() || list.count == 0 {
            return
        }
        
        if local {
            self.planLocalNotifications(container: container, list: list)
        }
        else{
            self.planRemoteNotifications(container: container, list: list)
        }
    }
    
    // Send by local notifications per container (make sure the list is complete, previous lists are discarded)
    private static func planLocalNotifications(container: String, list: [NotificationItem]){
        for (index, item) in list.enumerated() {
            let content = UNMutableNotificationContent()
            content.title = item.title
            content.body = item.body
            content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: item.sound))
            content.badge = item.badge as NSNumber
            
            let date = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: Date(timeIntervalSince1970: item.time))
            let trigger = UNCalendarNotificationTrigger(dateMatching: date, repeats: false)
            let request = UNNotificationRequest(identifier: "\(container)-\(index)", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request)
        }
    }
    
    // Send by remote notifications per container (make sure the list is complete, previous lists are discarded)
    private static func planRemoteNotifications(container: String, list: [NotificationItem]){
        let jsonEncoder = JSONEncoder()
        if let jsonData = try? jsonEncoder.encode(list),
            let json = String(data: jsonData, encoding: .utf8) {
            // This is send to the storage api, it knows what devices are linked to what containers
            RequestQueue.shared.addToQueue(conf: Conf().storage, path: "/setnotifications", array: ["container" : container, "list": json])
        }
    }
    
    // Remove both local and remote notifications to prevent duplicates
    private static func removeLocalNotifications(container: String){
        // Local
        let center = UNUserNotificationCenter.current()
        center.getPendingNotificationRequests { (notifications) in
            for item in notifications.filter({$0.identifier.range(of:container) != nil}) {
                center.removePendingNotificationRequests(withIdentifiers: [item.identifier])
            }
        }
    }
    
    // Remove both local and remote notifications to prevent duplicates
    private static func removeRemoteNotifications(container: String){
        // Remote
        RequestQueue.shared.addToQueue(conf: Conf().storage, path: "/delnotifications", array: ["container" : container, "list": "[]"])
    }
    
    // What to do with notifications we receive?
    @objc public static func received(userInfo: [AnyHashable : Any]) {
        if let aps = userInfo["aps"] as? [String: AnyObject],
            let category = aps["category"] as? String {
            
            if category == "add-file" {
                if let data = userInfo["data"] as? [String: AnyObject],
                    let container = data["container"] as? String,
                    let file = data["file"] as? String {
                    StorageFiles.shared.getRemoteFile(container: container, file: file)
                }
            }
            else if category == "delete-file" {
                if let data = userInfo["data"] as? [String: AnyObject],
                    let container = data["container"] as? String,
                    let file = data["file"] as? String {
                    StorageFiles.shared.deleteFile(container: container, file: file)
                }
            }
        }
        
        if let identifier = userInfo["identifier"] as? String {
            RequestQueue.shared.addToQueue(conf: Conf().notifications, path: "/received", array: ["identifier": identifier])
        }
    }
    
    //MAKR: Delegates
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        Notifications.received(userInfo: notification.request.content.userInfo)
        completionHandler([.banner, .list, .sound])
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void){
        Notifications.received(userInfo: response.notification.request.content.userInfo)
        completionHandler()
    }
    
}


