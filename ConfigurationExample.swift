//
//  ConfigurationExample.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 21/10/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import Foundation

struct ConfigurationItem : Codable {
    let url : String
    let key : String
    let secret : String
}

//Rename file and add it to the target
struct Conf : Codable {
    let dir = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier:"")
    let defaultContainerName = "New Container"
    
    #if DEBUG
    let notificationsEnvironment = "apns_dev"
    #else
    let notificationsEnvironment = "apns"
    #endif
    
    let storage = ConfigurationItem(
        url: "https://api.domain.com/storage",
        key: "",
        secret: ""
    )
    let notifications = ConfigurationItem(
        url: "https://api.domain.com/notifications",
        key: "",
        secret: ""
    )
    let statistics = ConfigurationItem(
        url: "https://api.domain.com/statistics",
        key: "",
        secret: ""
    )
}
