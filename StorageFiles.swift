//
//  StorageFiles.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 24/10/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import Foundation
import CryptoKit

final class StorageFiles {
    static let shared = StorageFiles()
    
    // We have multiple actions to do, so keep track of the amount of running actions
    private var busyCounter = 0
    
    var dir = Conf().dir
    
    // Add a container
    public func addContainer(container: String) {
        if let dir = dir {
            let dirUrl = dir.appendingPathComponent(container)

            //Get only non-hidden files
            if let filelist = try? FileManager.default.contentsOfDirectory(at: dirUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles).filter({!$0.hasDirectoryPath}) {
                for file in filelist {
                    addFile(container: container, file: file)
                }
            }
        }
    }
    
    // Add file (Encrypt it when sending to the server, only when set to non local)
    public func addFile(container: String, file: URL) {
        let name = file.lastPathComponent

        if let data = try? Data(contentsOf: file),
            let info = StorageContainers.shared.list.first(where: { $0.uuid == container }),
            !info.local {
            
            if let encrypted = encrypt(data: data, key: info.key, file: file) {
                RequestQueue.shared.addToQueue(conf: Conf().storage, path: "/addfile", array: ["container": container, "file": name, "data": encrypted.base64EncodedString()])
            }
        }
    }

    public func addFile(container: String, name: String, data: Data) {
        if let dir = dir {
            let url = dir.appendingPathComponent(container).appendingPathComponent(name)

            StorageContainers.shared.createFolder(uuid: container)
            
            try? data.write(to: url, options: .atomic)

            addFile(container: container, file: url)
        }
    }

    // Delete file
    public func deleteFile(container: String, file: URL) {
        RequestQueue.shared.addToQueue(conf: Conf().storage, path: "/deletefile", array: ["container": container, "file": file.lastPathComponent])
        
        try? FileManager.default.removeItem(at: file)
    }

    public func deleteFile(container: String, file: String) {
        if let dir = dir {
            let url = dir.appendingPathComponent(container).appendingPathComponent(file)
            deleteFile(container: container, file: url)
        }
    }
    
    // Move files from container to container
    public func moveFile(container: String, file: String, tocontainer: String) {
        let data = getFile(container: container, file: file)
        if data.count > 0 {
            addFile(container: tocontainer, name: file, data: data)
            deleteFile(container: container, file: file)
            
        }
    }

    // Gets a file from the container and return the data
    public func getFile(container: String, file: String) -> Data {
        if let dir = dir {
            let url = dir.appendingPathComponent(container).appendingPathComponent(file)
            
            if let data = try? Data(contentsOf: url){
                return data
            }
        }
        
        return Data()
    }

    
    // Synchronize container files
    public func syncFiles() {
        if busyCounter > 0 {
            return
        }
        
        // For all non local containers
        for container in StorageContainers.shared.list.filter({!$0.local}) {
            busyCounter += 1
            
            // Get online filelist
            RequestSend.send(conf: Conf().storage, path: "/listfiles", array: ["container": container.uuid]) { (data, response) -> () in
                let jsonDecoder = JSONDecoder()
                
                if let data = data,
                    let online = try? jsonDecoder.decode([[String:String]].self, from: data) {
                    
                    // Get offline filelist
                    let offline = self.listLocalFiles(container: container.uuid)

                    // Go compare
                    self.compareFiles(container: container.uuid, offline: offline, online: online)
                }
                
                self.busyCounter -= 1
            }
        }
    }
    
    private func listLocalFiles(container: String) -> [[String:String]] {
        
        // See if the files have changed
        if let dir = dir {

            let dirUrl = dir.appendingPathComponent(container)
            let fileUrl = dirUrl.appendingPathComponent(".md5.json")

            // If different, create a new .md5 file
            var changed = true
            if let dirTime = fileModificationDate(url: dirUrl),
                let fileTime = fileModificationDate(url: fileUrl),
                Int(dirTime.timeIntervalSince(fileTime)) == 0 {
                changed = false
            }
            
            if changed {
                var list : [[String:String]] = []
                
                //Get only non-hidden files
                if let filelist = try? FileManager.default.contentsOfDirectory(at: dirUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles).filter({!$0.hasDirectoryPath}),
                    let info = StorageContainers.shared.list.first(where: { $0.uuid == container }) {
                    for file in filelist {
                        let filename = file.absoluteURL.lastPathComponent
                        let filemd5 = md5(file: file, key: info.key)
                        
                        list.append(["file" : filename, "md5" : filemd5])
                    }
                }

                // Store list to folder
                let jsonEncoder = JSONEncoder()
                if let jsonData = try? jsonEncoder.encode(list) {
                    try? FileManager.default.removeItem(at: dirUrl.appendingPathComponent(".md5.json"))
                    try? jsonData.write(to: dirUrl.appendingPathComponent(".md5.json"), options: .completeFileProtection)
                }
                return list
            }

            // Return the md5 file
            let jsonDecoder = JSONDecoder()
            if let jsonData = try? Foundation.Data(contentsOf: dirUrl.appendingPathComponent(".md5.json"), options: .mappedIfSafe),
                let list = try? jsonDecoder.decode([[String:String]].self, from: jsonData){
                return list
            }
        }
        
        return []
    }
    
    private func compareFiles(container: String, offline: [[String:String]], online: [[String:String]]) {
        if let dir = dir {
            let dirUrl = dir.appendingPathComponent(container)

            // Files to delete
            for offfile in offline {
                let index = online.firstIndex(where: { $0["file"] == offfile["file"] })
                if index == nil,
                    let file = offfile["file"] {
                    try? FileManager.default.removeItem(at: dirUrl.appendingPathComponent(file))
                }
            }
        
            // Files to download
            for onfile in online {
                let index = offline.firstIndex(where: { $0["file"] == onfile["file"] && $0["md5"] == onfile["md5"] })
                if index == nil,
                    let file = onfile["file"] {
                    getRemoteFile(container: container, file: file)
                }
            }
        }
    }
    
    public func getRemoteFile(container: String, file: String){
        RequestSend.send(conf: Conf().storage, path: "/getfile", array: ["container": container, "file": file]) { (data, response) -> () in
            if let data = data,
                let dir = self.dir,
                let info = StorageContainers.shared.list.first(where: { $0.uuid == container }),
                let decrypted = self.decrypt(data: data, key: info.key){
                let url = dir.appendingPathComponent(container).appendingPathComponent(file)
                
                StorageContainers.shared.createFolder(uuid: container)
                try? decrypted.write(to: url, options: .atomic)

                // If we downloaded a new json file, notify anybody listening
                if url.pathExtension == "json" {
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: Notification.Name("CloudStorageFileDownloadedForContainer"), object: self, userInfo: ["container": container, "file": file])
                    }
                }
            }
        }
    }

    private func fileModificationDate(url: URL) -> Date? {
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: url.path)
            return attr[FileAttributeKey.modificationDate] as? Date
        } catch {
            return nil
        }
    }
    
    // We want the md5 from a file to compare with server data, but the server data is encrypted, so do the same here
    private func md5(file: URL, key: String) -> String {
        if let data = try? Data(contentsOf: file),
            let encrypted = encryptraw(data: data, key: key, file: file) {

            // https://stackoverflow.com/a/56578995
            let digest = Insecure.MD5.hash(data: encrypted)
            return digest.map {String(format: "%02hhx", $0)}.joined()
        }
        return ""
    }
    
    private func encrypt(data: Data, key: String, file: URL) -> Data? {
        guard let keydata = key.data(using: .utf8) else {
            return Data()
        }
        
        let keyData = SHA256.hash(data: keydata)
        let randomKey = SymmetricKey(data: keyData)

        if let cryptedBox = try? ChaChaPoly.seal(data, using: randomKey),
            let sealedBox = try? ChaChaPoly.SealedBox(combined: cryptedBox.combined) {
            
            let encryptedData = sealedBox.combined
            let base64encoded = encryptedData.base64EncodedData()

            let encryptedurl = file.deletingLastPathComponent().appendingPathComponent("." + file.lastPathComponent)
            try? base64encoded.write(to: encryptedurl)
            
            return encryptedData.base64EncodedData()
        }
        
        return Data()
    }

    private func decrypt(data: Data, key: String) -> Data? {
        guard let keydata = key.data(using: .utf8),
            let boxdata = Data(base64Encoded: data) else {
            return Data()
        }
        
        let keyData = SHA256.hash(data: keydata)
        let randomKey = SymmetricKey(data: keyData)
        
        if let sealedBoxToOpen = try? ChaChaPoly.SealedBox(combined: boxdata),
            let decryptedData = try? ChaChaPoly.open(sealedBoxToOpen, using: randomKey) {
            return decryptedData
        }
        
        return Data()
    }
    
    private func encryptraw(data: Data, key: String, file: URL) -> Data? {
        guard let keydata = key.data(using: .utf8) else {
            return Data()
        }

        let keyData = SHA256.hash(data: keydata)
        let randomKey = SymmetricKey(data: keyData)

        let encryptedurl = file.deletingLastPathComponent().appendingPathComponent("." + file.lastPathComponent)

        if let base64data = try? Data(contentsOf: encryptedurl),
            let boxdata = Data(base64Encoded: base64data),
            let localBoxToOpen = try? ChaChaPoly.SealedBox(combined: boxdata){
            
            let sealedBox = try! ChaChaPoly.seal(data, using: randomKey, nonce: localBoxToOpen.nonce)
            let encryptedData = sealedBox.combined
            
            return encryptedData.base64EncodedData()
        }
        
        return Data()
    }
}
