//
//  Statistics.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 09/12/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import UIKit

public final class Statistics: NSObject {
    static let shared = Statistics()
    
    private var metrics = Metrics()
    private var lastpath = ""
    private var lastinfo = ""

    @objc public static func addPath(path: String, info: String){
        var pathItem = PathItem()
        pathItem.path = path
        pathItem.info = info

        Statistics.shared.metrics.paths.append(pathItem)
        Statistics.shared.saveStatistics()
        Statistics.shared.lastpath = path
        Statistics.shared.lastinfo = info
    }
    
    @objc public static func addAction(action: String, info: String){
        var actionItem = ActionItem()
        actionItem.action = action
        actionItem.info = info

        Statistics.shared.metrics.actions.append(actionItem)
        Statistics.shared.saveStatistics()
    }

    override init(){
        super.init()
        readStatistics()
    }
    
    // Send our metrics
    public func send(){
        // Do we have any to send?
        if metrics.paths.count == 0 && metrics.actions.count == 0 {
            return
        }

        // Add additional metrics
        metrics.metrics.append(application())
        metrics.metrics.append(device())
        metrics.metrics.append(locale())
        metrics.metrics.append(operatingsystem())

        // Convert to json and send away
        let jsonEncoder = JSONEncoder()
        if let jsonData = try? jsonEncoder.encode(metrics),
            let json = String(data: jsonData, encoding: .utf8) {
            RequestQueue.shared.addToQueue(conf: Conf().statistics, path: "/add", array: ["data": json])
        }

        // Reset for some new statistics
        self.metrics = Metrics()
        Statistics.shared.saveStatistics()
    }

    // Resuming from the background, we guess this is the screen ;-)
    public func resume(){
        Statistics.addPath(path: lastpath, info: lastinfo)
    }
    
    private func application() -> MetricsItem {
        var metric = MetricsItem()
        if let bundle = Bundle.main.infoDictionary {
            metric.type = "application"
            metric.value1 = bundle["CFBundleName"] as! String
            metric.value2 = bundle["CFBundleShortVersionString"] as! String
        }
        return metric
    }

    private func device() -> MetricsItem {
        //With the help of  https://stackoverflow.com/a/11197770
        func deviceModel() -> String {
            var systemInfo = utsname()
            uname(&systemInfo)
            let machineMirror = Mirror(reflecting: systemInfo.machine)
            let identifier = machineMirror.children.reduce("") { identifier, element in
                guard let value = element.value as? Int8, value != 0 else {
                    return identifier
                }
                return identifier + String(UnicodeScalar(UInt8(value)))
            }
            return identifier
        }
        
        return MetricsItem(type: "device", value1: "Apple", value2: deviceModel())
    }
    
    private func locale() -> MetricsItem {
        var metric = MetricsItem()
        metric.type = "locale"
        let locale = getPreferredLocale()
        if let language = locale.languageCode {
            metric.value1 = language.lowercased()
        }
        if let region = locale.regionCode {
            metric.value2 = region.lowercased()
        }
        return metric
    }
    
    private func getPreferredLocale() -> Locale {
        guard let preferredIdentifier = Locale.preferredLanguages.first else {
            return Locale.current
        }
        return Locale(identifier: preferredIdentifier)
    }
    
    private func operatingsystem() -> MetricsItem {
        var metric = MetricsItem()
        metric.type = "operatingsystem"
        metric.value1 = UIDevice.current.systemName
        metric.value2 = UIDevice.current.systemVersion
        return metric
    }
    
    // Save the remaining statistics in case of app stop/crash
    private func saveStatistics(){
        let jsonEncoder = JSONEncoder()
        if let jsonData = try? jsonEncoder.encode(self.metrics),
            let dir = Conf().dir {
            let fileURL = dir.appendingPathComponent(".statistics.json")
            
            try? jsonData.write(to: fileURL, options: .completeFileProtection)
        }
    }
    
    // Read the statistics from file, if it exist
    private func readStatistics(){
        if let dir = Conf().dir {
            let fileURL = dir.appendingPathComponent(".statistics.json")
            let jsonDecoder = JSONDecoder()
            if let jsonData = try? Data(contentsOf: fileURL, options: .mappedIfSafe),
                let metrics = try? jsonDecoder.decode(Metrics.self, from: jsonData){
                self.metrics = metrics
            }
        }
    }
}

fileprivate struct Metrics: Codable {
    var paths : [PathItem] = []
    var actions : [ActionItem] = []
    var metrics : [MetricsItem] = []
}

fileprivate struct PathItem: Codable {
    var timestamp = NSDate().timeIntervalSince1970
    var path = ""
    var info = ""
}

fileprivate struct ActionItem: Codable {
    var timestamp = NSDate().timeIntervalSince1970
    var action = ""
    var info = ""
}

fileprivate struct MetricsItem: Codable {
    var type = ""
    var value1 = ""
    var value2 = ""
}
