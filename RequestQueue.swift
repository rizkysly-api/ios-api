//
//  RequestQueue.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 24/10/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import Foundation

// Everything we send go thhrough the queue. Just in case we have network mishaps

struct QueueItem: Codable {
    var conf = ConfigurationItem(url: "", key: "", secret: "")
    var path : String = ""
    var array : [String:String] = [:]
}

final class RequestQueue {
    static let shared = RequestQueue()
    
    private var queue : [QueueItem] = []
    public var busy = false
    
    init(){
        // Read the queue from file
        readQueue()
    }
    
    public func inqueue(conf: ConfigurationItem) -> Bool {
        return queue.filter{ $0.conf.url == conf.url }.count > 0
    }
    
    // What information do we need to sync? Add it to the queue and send away
    public func addToQueue(conf: ConfigurationItem, path: String, array: [String: String]){
        let queueItem = QueueItem(conf: conf, path: path, array: array)
        queue.append(queueItem)
        sendQueue()
    }
    
    // Send the entire queue to the server
    public func sendQueue(){
        if !busy {
            busy = true
            sendItem()
        }
    }
    
    // Send all items in the queue to the server, the order of the queue is important
    private func sendItem(){
        // Nothing in queue? Then stop doing this
        guard let queueItem = queue.first else{
            busy = false
            return
        }
        
        // Create the request
        RequestSend.send(conf: queueItem.conf, path: queueItem.path, array: queueItem.array) { (data, response) -> () in
            if data == nil || response == nil {
                self.busy = false
                return
            }
            
            if let response = response {
                // 500 errors are server errors, and we have to try again later
                if response.statusCode >= 500 {
                    self.busy = false
                    return
                }
                
                // the rest will be reverted to the latest server version on app restart
            }
            
            // All is succesfull, so remove the item from the queue
            self.queue.removeFirst()
            
            // Save the remaining queue in case of app stop/crash
            self.saveQueue()
            
            // Send the next queued item to the server
            self.sendItem()
        }
    }
    
    // Save the remaining queue in case of app stop/crash
    private func saveQueue(){
        let jsonEncoder = JSONEncoder()
        if let jsonData = try? jsonEncoder.encode(self.queue),
            let dir = Conf().dir {
            let fileURL = dir.appendingPathComponent(".queue.json")
            
            try? jsonData.write(to: fileURL, options: .completeFileProtection)
        }
    }
    
    // Read the queue from file, if it exist
    private func readQueue(){
        if let dir = Conf().dir {
            
            let fileURL = dir.appendingPathComponent(".queue.json")
            let jsonDecoder = JSONDecoder()
            if let jsonData = try? Foundation.Data(contentsOf: fileURL, options: .mappedIfSafe),
                let queue = try? jsonDecoder.decode([QueueItem].self, from: jsonData){
                self.queue = queue
            }
        }
    }
}
