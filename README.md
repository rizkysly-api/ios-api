# iOS API - Swift
API classes for communicating with the storage, notification and statistics APIs.

## Installation
Within your xcode project.
`git clone https://gitlab.com/rizkysly-api/ios-api.git`  

Add the files to your project.

Rename `ConfigurationExample.swift` to Configuration.swift, change the files constants and add it to the target.  

Add this to your AppDelegate
```swift
func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    RizkySlyAPI.shared.start()
    return true
}
func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    Notifications.store(tokenData: deviceToken)
}
func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    Notifications.received(userInfo: userInfo)
    completionHandler(.newData)
}
```

## Notifications
Handles notification stuff  

```swift
// Plan notifications, see Storage API - PHP for list details
Notifications.planNotifications(local: Bool, container: String, list: [NotificationItem])
```

## RequestQueue
Queue all requests we want to sent to the server.  
```swift
// Add an item to the queue
RequestQueue.shared.addToQueue(conf: ConfigurationItem, path: String, array: [String: String])
```

## RequestSend
Sends requests to the server
```swift
// Send the request
RequestSend.send(conf: ConfigurationItem, path: String, array: [String: String], completion:())

// The same, but without a completion handler (send and forget)
RequestSend.send(conf: ConfigurationItem, path: String, array: [String: String])
```

## Statistics
Send statistics to the server
```swift
// Adds a path or screen view to the statistics we want to send to the server  
Statistics.shared.addPath(path: String, info: String)

// Adds an action to the statistics we want to send to the server  
Statistics.shared.addAction(action: String, info: String)  
```

## StorageContainers
We store data in containers so apps can have multiple containers/lists/items/just give it a name... 
```swift
StorageContainers.shared.addContainer(name: String)
StorageContainers.shared.renameContainer(uuid: String, name: String) 
StorageContainers.shared.shareContainer(uuid: String)
StorageContainers.shared.resetContainer(uuid: String)
StorageContainers.shared.deleteContainer(uuid: String)
StorageContainers.shared.qrContainer(uuid: String, width: CGFloat)
```

## StorageFiles
Containers contain files
```swift
// You have stored the file in the correct container and you want to let the app know it needs sending to the server 
StorageFiles.shared.addFile(container: String, file: URL)

// Just give the filedata and this function will store it in the right container and send it to the server
StorageFiles.shared.addFile(container: String, name: String, data: Data)

// Remove a file based on the complete URL
StorageFiles.shared.deleteFile(container: String, file: URL)

// Remove a file based on its name
StorageFiles.shared.deleteFile(container: String, file: String)
```
